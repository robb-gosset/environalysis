/*
 * sensors.cpp
 *
 *  Created on: 7 Aug 2014
 *      Author: root
 */

#include <SFE_BMP180.h>
#include <SFE_TSL2561.h>
#include "Arduino.h"
#include <Wire.h>
#include "cozir.h"
#include "AltSoftSerial.h"

#include "sensors.h"

sensors::sensors() {
windSensorPin = 0;
}

void sensors::powerup () {

	// Initialize the sensor (it is important to get calibration values stored on the device).

	if (pressure.begin())
		Serial.println("BMP180 init success");
	else {
		Serial.println("BMP180 init fail\n\n");
		while (1)
			; // Pause forever.
	}

	carbox.begin();

	light.begin();

	unsigned char ID;

	if (this->light.getID(ID)) {
		Serial.print("Got factory ID: 0X");
		Serial.print(ID, HEX);
		Serial.println(", should be 0X5X");
	} else {
		Serial.println("TSL2561 init fail\n\n");
		while (1)
			;
	}

	gain = 0;

	// If time = 0, integration will be 13.7ms
	// If time = 1, integration will be 101ms
	// If time = 2, integration will be 402ms
	// If time = 3, use manual start / stop to perform your own integration

	unsigned char time = 2;

	// setTiming() will set the third parameter (ms) to the
	// requested integration time in ms (this will be useful later):

	Serial.println("Set timing...");
	this->light.setTiming(gain, time, ms);

	// To start taking measurements, power up the sensor:

	Serial.println("Powerup...");
	this->light.setPowerUp();
	Serial.println("All sensors functional and online.");
}

void sensors::poll () {

	  // Loop here getting pressure readings every 10 seconds.


	  status = pressure.startTemperature();
	  if (status != 0)
	  {
	    // Wait for the measurement to complete:
	    delay(status);

	    // Retrieve the completed temperature measurement:
	    // Note that the measurement is stored in the variable T.
	    // Function returns 1 if successful, 0 if failure.

	    status = pressure.getTemperature(T);
	    if (status != 0)
	    {
	      // Print out the measurement:
	      Serial.print("temperature: ");
	      Serial.print(T,2);
	      Serial.print(" deg C, ");

	      // Start a pressure measurement:
	      // The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
	      // If request is successful, the number of ms to wait is returned.
	      // If request is unsuccessful, 0 is returned.

	      status = pressure.startPressure(3);
	      if (status != 0)
	      {
	        // Wait for the measurement to complete:
	        delay(status);

	        // Retrieve the completed pressure measurement:
	        // Note that the measurement is stored in the variable P.
	        // Note also that the function requires the previous temperature measurement (T).
	        // (If temperature is stable, you can do one temperature measurement for a number of pressure measurements.)
	        // Function returns 1 if successful, 0 if failure.

	        status = pressure.getPressure(P,T);
	        if (status != 0)
	        {
	          // Print out the measurement:
	          Serial.print("absolute pressure: ");
	          Serial.print(P,2);
	          Serial.print(" mb, ");
	          Serial.println();

	        }
	        else Serial.println("error retrieving pressure measurement\n");
	      }
	      else Serial.println("error starting pressure measurement\n");
	    }
	    else Serial.println("error retrieving temperature measurement\n");
	  }
	  else Serial.println("error starting temperature measurement\n");


	  unsigned int data0, data1;

	  if (light.getData(data0,data1))
	  {

	    // To calculate lux, pass all your settings and readings
	    // to the getLux() function.

	    // The getLux() function will return 1 if the calculation
	    // was successful, or 0 if one or both of the sensors was
	    // saturated (too much light). If this happens, you can
	    // reduce the integration time and/or gain.
	    // For more information see the hookup guide at: https://learn.sparkfun.com/tutorials/getting-started-with-the-tsl2561-luminosity-sensor

	    double lux;    // Resulting lux value
	    boolean good;  // True if neither sensor is saturated

	    // Perform lux calculation:

	    good = light.getLux(gain,ms,data0,data1,lux);

	    // Print out the results:

	    Serial.print("lux: ");
	    Serial.print(lux);
	    if (good) Serial.println(" (good)"); else Serial.println(" (BAD)");
	  }
	  else
	  {
	    // getData() returned false because of an I2C error, inform the user.

	    byte error = light.getError();
	    printError(error);
	  }


	  Serial.print("Occupancy: ");
	  if (state)
	  {
	   Serial.println("Not sensed.");
	  } else {
	   Serial.println("Sensed.");
	  }
	  state = TRUE;

	  Serial.print("Air Movement: ");
	  Serial.print(analogRead(windSensorPin));
	  Serial.println("/1024");

	  delay(1000);  // Pause for 5 seconds.
}

sensors::~sensors ()
{
  
}

void printError(byte error)
// If there's an I2C error, this function will
// print out an explanation.
		{
	Serial.print("I2C error: ");
	Serial.print(error, DEC);
	Serial.print(", ");

	switch (error) {
	case 0:
		Serial.println("success");
		break;
	case 1:
		Serial.println("data too long for transmit buffer");
		break;
	case 2:
		Serial.println("received NACK on address (disconnected?)");
		break;
	case 3:
		Serial.println("received NACK on data");
		break;
	case 4:
		Serial.println("other error");
		break;
	default:
		Serial.println("unknown error");
	}
}
