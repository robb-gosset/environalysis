/*
 * sensors.h
 *
 *  Created on: 7 Aug 2014
 *      Author: root
 */

#ifndef SENSORS_H_
#define SENSORS_H_

#define TRUE 1
#define FALSE 0

#include <SFE_BMP180.h>
#include <SFE_TSL2561.h>
#include "Arduino.h"
#include "cozir.h"
#include "sensorReading.h"


class sensors {
public:
	sensors();
	~sensors();
	void poll ();
	void powerup ();

private:
	SFE_BMP180 pressure;
	SFE_TSL2561 light;
	cozir carbox;

	int windSensorPin;

	// variables for light sensor
	boolean gain;     // Gain setting, 0 = X1, 1 = X16;
	unsigned int ms;  // Integration ("shutter") time in milliseconds

	// variables for pressure sensor
	char status;
	char state;
	double T, P, p0, a;

	void printError(byte error);

};

#endif /* SENSORS_H_ */
