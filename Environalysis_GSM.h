// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _Environalysis_GSM_H_
#define _Environalysis_GSM_H_
#include "Arduino.h"
//add your includes for the project Environalysis_GSM here

//end of add your includes here
#ifdef __cplusplus
extern "C" {
#endif
void loop();
void setup();
#ifdef __cplusplus
} // extern "C"
#endif

//add your function definitions for the project Environalysis_GSM here

// Important
#define BAUDRATE 57600

//// LED Pins
//#define ledGreen 13
//#define ledRed 12

// GSM Defines
#define PINNUMBER ""
#define GPRS_APN       "GPRS_APN" // replace your GPRS APN
#define GPRS_LOGIN     "login"    // replace with your GPRS login
#define GPRS_PASSWORD  "password" // replace with your GPRS password

// General Defines
#define FALSE 0
#define TRUE 1

//Do not add code below this line
#endif /* _Environalysis_GSM_H_ */
