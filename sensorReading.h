/*
 * sensorReading.h
 *
 *  Created on: 28 Aug 2014
 *      Author: robb
 */

#ifndef SENSORREADING_H_
#define SENSORREADING_H_

class sensorReading {
public:
	sensorReading();
	~sensorReading();

	void setTemp180 (float newTemp);
	float getTemp180 ();

	void setPressure (float newPressure);
	float getPressure ();

	void setLight (float newLight);
	float getLight ();

	void setOccupancy (bool newOccupancy);
	bool getOccupancy ();

	void setAirMovement (int newAirMovement);
	int getAirMovement ();

	void setCO2Conc (int newCO2Conc);
	int getCO2Conc ();

	void setTempCOZ (float newTempCOZ);
	float getTempCOZ ();

	void setHumidity (float newHumidity);
	float getHumidity ();

private:

	float temp180;   // BPM180
	float pressure;  // BMP180

	float light;     // TLS2501

	bool occupancy;  // PIR
	int airmovement; // wind sensor

	int co2ConcCOZ;  // Cozir CO2 sensor
	float tempCOZ;     // Cozir temp sensor
	float humidityCOZ; // Cozir humidity sensor
};




#endif /* SENSORREADING_H_ */
