/*
 * cozir.h
 *
 *  created on: 18 aug 2014
 *      author: robb
 */

#ifndef cozir_h_
#define cozir_h_
#include <AltSoftSerial.h>
#include "sensors.h"

#define true 1
#define false 0

#define cozir_rx 11
#define cozir_tx 10


class cozir {
public:
	cozir ();
	~cozir();
int begin ();
int getReadings (sensorReading* currentReading);

private:
	AltSoftSerial cozirSerial;

};



#endif /* cozir_h_ */
